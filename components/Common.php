<?php


namespace app\components;


class Common
{
    public static function initDates($date): array
    {
        $dateMonth = date('m', $date);
        $dateYear = date('Y', $date);
        $dateInit = strtotime("{$dateYear}-{$dateMonth}-01");
        $dateMiddle = strtotime("{$dateYear}-{$dateMonth}-15");
        $period = 0; // Quincena 1

        if ($date < $dateMiddle) {
            $dateRegister = date('Y-m-d', $dateInit);
            $dateEnd = date('Y-m-d', $dateMiddle);
        } else {
            $dateRegister = date('Y-m-d', $dateMiddle);
            $dateEnd = date('Y-m-t', $date);
            $period = 1; // Quincena 2
        }

        return [
            'dateRegister' => $dateRegister,
            'dateEnd' => $dateEnd,
            'period' => $period,
            'dateInit' => $dateInit,
            'dateMiddle' => $dateMiddle,
        ];
    }
}