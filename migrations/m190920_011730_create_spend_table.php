<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%spend}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 */
class m190920_011730_create_spend_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%spend}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(150)->notNull(),
            'price' => $this->double()->notNull(),
            'date' => $this->date()->notNull(),
            'userId' => $this->integer(),
        ]);

        // creates index for column `userId`
        $this->createIndex(
            '{{%idx-spend-userId}}',
            '{{%spend}}',
            'userId'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-spend-userId}}',
            '{{%spend}}',
            'userId',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-spend-userId}}',
            '{{%spend}}'
        );

        // drops index for column `userId`
        $this->dropIndex(
            '{{%idx-spend-userId}}',
            '{{%spend}}'
        );

        $this->dropTable('{{%spend}}');
    }
}
