<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cron_spend}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 */
class m190919_183047_create_cron_spend_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cron_spend}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(150)->notNull(),
            'price' => $this->double()->notNull(),
            'date' => $this->date()->notNull(),
            'reminderDay' => $this->integer()->notNull(),
            'userId' => $this->integer(),
        ]);

        // creates index for column `userId`
        $this->createIndex(
            '{{%idx-cron_spend-userId}}',
            '{{%cron_spend}}',
            'userId'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-cron_spend-userId}}',
            '{{%cron_spend}}',
            'userId',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-cron_spend-userId}}',
            '{{%cron_spend}}'
        );

        // drops index for column `userId`
        $this->dropIndex(
            '{{%idx-cron_spend-userId}}',
            '{{%cron_spend}}'
        );

        $this->dropTable('{{%cron_spend}}');
    }
}
