<?php

use yii\db\Migration;

/**
 * Class m190919_143813_alter_user_table
 */
class m190919_143813_alter_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('user', 'token', $this->string(255)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190919_143813_alter_user_table cannot be reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190919_143813_alter_user_table cannot be reverted.\n";

        return false;
    }
    */
}
