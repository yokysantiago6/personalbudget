<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%budget}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 */
class m190919_140820_create_budget_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%budget}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(150)->notNull(),
            'price' => $this->double()->notNull(),
            'period' => $this->integer()->notNull(),
            'reminderDay' => $this->integer()->notNull(),
            'userId' => $this->integer()->notNull(),
        ]);

        // creates index for column `userId`
        $this->createIndex(
            '{{%idx-budget-userId}}',
            '{{%budget}}',
            'userId'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-budget-userId}}',
            '{{%budget}}',
            'userId',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-budget-userId}}',
            '{{%budget}}'
        );

        // drops index for column `userId`
        $this->dropIndex(
            '{{%idx-budget-userId}}',
            '{{%budget}}'
        );

        $this->dropTable('{{%budget}}');
    }
}
