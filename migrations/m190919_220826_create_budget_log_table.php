<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%budget_log}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 */
class m190919_220826_create_budget_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%budget_log}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(150)->notNull(),
            'price' => $this->double()->notNull(),
            'date' => $this->date()->notNull(),
            'reminderDay' => $this->integer()->notNull(),
            'salary' => $this->double()->notNull(),
            'extra' => $this->double()->notNull(),
            'userId' => $this->integer(),
        ]);

        // creates index for column `userId`
        $this->createIndex(
            '{{%idx-budget_log-userId}}',
            '{{%budget_log}}',
            'userId'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-budget_log-userId}}',
            '{{%budget_log}}',
            'userId',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-budget_log-userId}}',
            '{{%budget_log}}'
        );

        // drops index for column `userId`
        $this->dropIndex(
            '{{%idx-budget_log-userId}}',
            '{{%budget_log}}'
        );

        $this->dropTable('{{%budget_log}}');
    }
}
