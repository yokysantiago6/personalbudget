<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "budget_log".
 *
 * @property int $id
 * @property string $name
 * @property double $price
 * @property string $date
 * @property int $reminderDay
 * @property double $salary
 * @property double $extra
 * @property int $userId
 *
 * @property User $user
 */
class BudgetLog extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'budget_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name', 'price', 'date', 'reminderDay', 'salary', 'extra'], 'required'],
            [['price', 'salary', 'extra'], 'number'],
            [['date'], 'safe'],
            [['reminderDay', 'userId'], 'integer'],
            [['name'], 'string', 'max' => 150],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
            'date' => 'Date',
            'reminderDay' => 'Reminder Day',
            'salary' => 'Salary',
            'extra' => 'Extra',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(Users::class, ['id' => 'userId']);
    }

    public function beforeValidate(): bool
    {
        $this->userId = Yii::$app->user->getId();
        return parent::beforeValidate();
    }


    public static function saveLog( $value, $dateRegister, $salary, $extra, $preview = false)
    {
        if($preview === false) {
            $log = new self();
        } else {
            $log = new \stdClass();
        }
        $log->name = $value->name;
        $log->price = $value->price;
        $log->date = $dateRegister;
        $log->reminderDay = $value->reminderDay;
        $log->salary = $salary;
        $log->extra = $extra;
        return $preview ? $log : $log->save();
    }
}
