<?php

namespace app\models;

use app\components\Common;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "cron_spend".
 *
 * @property int $id
 * @property string $name
 * @property double $price
 * @property string $date
 * @property int $reminderDay
 * @property int $userId
 *
 * @property User $user
 */
class CronSpend extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'cron_spend';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name', 'price', 'date', 'reminderDay'], 'required'],
            [['price'], 'number'],
            [['date'], 'safe'],
            [['reminderDay', 'userId'], 'integer'],
            [['name'], 'string', 'max' => 150],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
            'date' => 'Date',
            'reminderDay' => 'Reminder Day',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(Users::class, ['id' => 'userId']);
    }

    public function beforeValidate(): bool
    {
        $this->userId = Yii::$app->user->getId();
        $dates = Common::initDates(strtotime($this->date));
        $this->getReminderDay($dates);
        return parent::beforeValidate();
    }

    private function getReminderDay($dates): void
    {
        $date = strtotime($this->date);
        $date1 = new \DateTime(date('Y-m-d', strtotime($dates['dateRegister'])));
        $date2 = new \DateTime(date('Y-m-d', $date));
        $diff = $date1->diff($date2);
        $this->reminderDay = $diff->days;
    }

    public static function getCronSpendInDate($dates)
    {
        return self::find()
            ->andFilterWhere(['>=', 'date', $dates['dateRegister']])
            ->andFilterWhere(['<', 'date', $dates['dateEnd']])
            ->all();
    }
}
