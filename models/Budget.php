<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "budget".
 *
 * @property int $id
 * @property string $name
 * @property double $price
 * @property int $period
 * @property string $periodName
 * @property int $reminderDay
 * @property int $userId
 *
 * @property User $user
 */
class Budget extends ActiveRecord
{
    public $periodName;

    public const PERIODS = [
        0 => 'Quincena 1',
        1 => 'Quincena 2',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'budget';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name', 'price', 'period', 'reminderDay', 'userId'], 'required'],
            [['price'], 'number'],
            [['period', 'reminderDay', 'userId'], 'integer'],
            [['name'], 'string', 'max' => 150],
            [['periodName'], 'safe'],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'Código',
            'name' => 'Nombre',
            'price' => 'Precio',
            'period' => 'Periodo',
            'reminderDay' => 'Días recordatorio',
            'userId' => 'Usuario',
            'periodName' => 'Periodo',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(Users::class, ['id' => 'userId']);
    }

    public function beforeValidate(): bool
    {
        $this->userId = Yii::$app->user->getId();
        $this->setPeriodByName();
        return parent::beforeValidate();
    }

    private function setPeriodByName(): void
    {
        foreach (self::PERIODS as $key => $value) {
            if ( $value === $this->periodName ) {
                $this->period = $key;
            }
        }
    }

    public static function getBudgetByPeriod($period)
    {
        return self::find()
            ->where(['period' => $period])
            ->all();
    }
}
