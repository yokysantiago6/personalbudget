<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "spend".
 *
 * @property int $id
 * @property string $name
 * @property double $price
 * @property string $date
 * @property int $userId
 *
 * @property User $user
 */
class Spend extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'spend';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name', 'price', 'date'], 'required'],
            [['price'], 'number'],
            [['date'], 'safe'],
            [['userId'], 'integer'],
            [['name'], 'string', 'max' => 150],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
            'date' => 'Date',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(Users::class, ['id' => 'userId']);
    }

    public function beforeValidate(): bool
    {
        $this->userId = Yii::$app->user->getId();
        return parent::beforeValidate();
    }

}
