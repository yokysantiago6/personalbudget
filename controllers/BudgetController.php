<?php

namespace app\controllers;

use app\models\Budget;
use Yii;
use yii\base\DynamicModel;
use yii\base\InvalidConfigException;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;
use yii\data\ActiveDataFilter;

class BudgetController extends ActiveController
{
    /**
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        parent::init();
        Yii::$app->user->enableSession = false;
    }
    
    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['dataFilter'] = [
            'class' => ActiveDataFilter::class,
            'searchModel' => static function () {
                return (new DynamicModel(['period' => null, 'userId' => null]))
                    ->addRule('period', 'number')
                    ->addRule('userId', 'number');
            },
        ];
        
        return $actions;
    }

    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::class,
            'authMethods' => [
                HttpBasicAuth::class,
                HttpBearerAuth::class,
                QueryParamAuth::class,
            ],
        ];
        return $behaviors;
    }
    public $modelClass = Budget::class;

}
