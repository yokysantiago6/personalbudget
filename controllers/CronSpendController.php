<?php

namespace app\controllers;

use app\components\Common;
use app\models\CronSpend;
use app\models\CronSpendSearch;
use Yii;
use yii\base\DynamicModel;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;

class CronSpendController extends ActiveController
{
    /**
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        parent::init();
        Yii::$app->user->enableSession = false;
    }

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'searchCronSpendByDate'];

        return $actions;
    }

    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::class,
            'authMethods' => [
                HttpBasicAuth::class,
                HttpBearerAuth::class,
                QueryParamAuth::class,
            ],
        ];
        return $behaviors;
    }

    public $modelClass = CronSpend::class;

    public function searchCronSpendByDate(): ActiveDataProvider
    {
        $query = CronSpend::find();

        if( isset(Yii::$app->request->get('filter')['date']) ) {
            $date = strtotime(Yii::$app->request->get('filter')['date']);
            $dates = Common::initDates($date);

            $query->andFilterWhere(['>=', 'date', $dates['dateRegister']])
                ->andFilterWhere(['<', 'date', $dates['dateEnd']]);
        }

        if ( isset(Yii::$app->request->get('filter')['userId']) ) {
            $userId = strtotime(Yii::$app->request->get('filter')['userId']);
        } else {
            $userId = Yii::$app->user->getId();
        }
        $query->andWhere(['userId' => $userId]);

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}
