<?php

namespace app\controllers;

use app\components\Common;
use app\models\Budget;
use app\models\BudgetLog;
use app\models\CronSpend;
use Yii;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;

class BudgetLogController extends ActiveController
{
    /**
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        parent::init();
        Yii::$app->user->enableSession = false;
    }

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'searchCronSpendByDate'];

        return $actions;
    }

    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::class,
            'authMethods' => [
                HttpBasicAuth::class,
                HttpBearerAuth::class,
                QueryParamAuth::class,
            ],
        ];
        return $behaviors;
    }

    public $modelClass = BudgetLog::class;

    public function searchCronSpendByDate(): ActiveDataProvider
    {
        $query = BudgetLog::find();

        if ( isset(Yii::$app->request->get('filter')['date']) ) {
            $date = strtotime(Yii::$app->request->get('filter')['date']);
            $dates = Common::initDates($date);

            $query->andFilterWhere(['>=', 'date', $dates['dateRegister']])
                ->andFilterWhere(['<', 'date', $dates['dateEnd']]);
        }
        if ( isset(Yii::$app->request->get('filter')['userId']) ) {
            $userId = strtotime(Yii::$app->request->get('filter')['userId']);
        } else {
            $userId = Yii::$app->user->getId();
        }
        $query->andWhere(['userId' => $userId]);

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }

    public function actionMake(): array
    {
        $salary = Yii::$app->request->post('salary');
        $extra = Yii::$app->request->post('extra');
        $date = strtotime(Yii::$app->request->post('date'));

        $dates = Common::initDates($date);

        $saving = new \stdClass();
        $saving->name = 'Ahorro';
        $saving->price = $salary - 300000;
        $saving->reminderDay = 1;

        $budget = Budget::getBudgetByPeriod($dates['period']);
        foreach ($budget as $key => $value) {
            $saving->price -= $value->price;
            BudgetLog::saveLog($value, $dates['dateRegister'], $salary, $extra);
        }

        $spend =CronSpend::getCronSpendInDate($dates);
        foreach ($spend as $key => $value) {
            $saving->price -= $value->price;
            BudgetLog::saveLog($value, $dates['dateRegister'], $salary, $extra);
        }

        BudgetLog::saveLog($saving, $dates['dateRegister'], $salary, $extra);

        return [
            'status' => 'success'
        ];
    }

    public function actionPreview(): array
    {
        $salary = Yii::$app->request->post('salary');
        $extra = Yii::$app->request->post('extra');
        $date = strtotime(Yii::$app->request->post('date'));
        $items = [
            [
                'name' => 'Gastos varios',
                'price' => 300000,
                'reminderDay' => 1,
                'date' => $date,
                'salary' => $salary,
                'extra' => $extra
            ]
        ];

        $dates = Common::initDates($date);

        $budget = Budget::getBudgetByPeriod($dates['period']);
        foreach ($budget as $key => $value) {
            $items[] = BudgetLog::saveLog($value, $dates['dateRegister'], $salary, $extra, true);
        }

        $spend = CronSpend::getCronSpendInDate($dates);
        foreach ($spend as $key => $value) {
            $items[] = BudgetLog::saveLog($value, $dates['dateRegister'], $salary, $extra, true);
        }


        return $items;
    }

    public function actionVerify(): array
    {

        $date = strtotime(Yii::$app->request->get('date'));
        $dates = Common::initDates($date);

        return [
            'count' => BudgetLog::find()->where(['date' => $dates['dateRegister']])->count()
        ];
    }

    public function actionRestore(): array
    {

        $date = strtotime(Yii::$app->request->post('date'));
        $dates = Common::initDates($date);
        BudgetLog::deleteAll(['date' => $dates['dateRegister']]);

        return [
            'status' => 'success'
        ];
    }

}
